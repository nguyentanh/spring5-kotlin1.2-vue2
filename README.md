### I. ENVIRONMENT ###
- JDK 8
- NodeJS >= 4

### II. BUILD ###

- Build

```
$ cd ${PROJECT_HOME}/ui

$ npm install

$ npm build

$ Copy all files ${PROJECT_HOME}/ui/dist/* --> ${PROJECT_HOME}/src/main/resources/static/*

$ gradle bootRun
```

- Browser
```
Open http://localhost:8080
```