package com.example.demo.handlers

import com.example.demo.model.Product
import com.example.demo.service.HomeService
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import reactor.core.publisher.Mono

@Component
class HomeHandler(val homeService: HomeService) {

    fun getProducts(serverRequest: ServerRequest): Mono<ServerResponse> {
        return ok().body(homeService.getProducts(), Product::class.java)
    }

    fun findProductById(serverRequest: ServerRequest): Mono<ServerResponse> {
        val id = serverRequest.pathVariable("id")
        return ok().body(homeService.findById(id.toInt()), Product::class.java)
    }

}