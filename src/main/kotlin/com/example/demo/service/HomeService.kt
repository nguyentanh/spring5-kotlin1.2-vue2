package com.example.demo.service

import com.example.demo.model.Product
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.math.BigDecimal

@Service
class HomeService {

    private fun createProducts(): MutableList<Product> {

        val products: MutableList<Product> = mutableListOf()
        products.add(Product(1, "A1", BigDecimal(0.1)))
        products.add(Product(2, "A2", BigDecimal(0.1)))
        products.add(Product(3, "A3", BigDecimal(0.6)))
        products.add(Product(4, "A4", BigDecimal(0.1)))
        products.add(Product(5, "A5", BigDecimal(1.1)))
        products.add(Product(6, "A6", BigDecimal(0.1)))
        products.add(Product(7, "A7", BigDecimal(9.1)))
        products.add(Product(8, "A8", BigDecimal(0.6)))
        products.add(Product(9, "A9", BigDecimal(0.13)))
        products.add(Product(10, "A10", BigDecimal(3.1)))
        products.add(Product(11, "A11", BigDecimal(0.1)))

        return products
    }

    fun getProducts(): Flux<Product> {

        return Flux.fromIterable(this.createProducts())
    }

    fun findById(id: Int): Mono<Product> {

        val products: MutableList<Product> = this.createProducts();
        val p = products[id]

        return Mono.just(p)
    }
}