package com.example.demo.router

import com.example.demo.handlers.HomeHandler
import org.apache.commons.io.IOUtils
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ResourceLoader
import org.springframework.http.MediaType
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.router
import java.nio.charset.Charset

@Configuration
class Router {

    @Bean
    fun apiRouter(homeHandler: HomeHandler) = router {
        (accept(MediaType.APPLICATION_JSON) and "/api").nest {
            "/product".nest {
                GET("/", homeHandler::getProducts)
                GET("/{id}", homeHandler::findProductById)
            }
        }
    }

    @Bean
    fun webRouter(homeHandler: HomeHandler, resourceLoader: ResourceLoader) = router {
        val resource = resourceLoader.getResource("classpath:/static/index.html")
        val content = IOUtils.toString(resource.inputStream, Charset.defaultCharset())
        accept(TEXT_HTML).nest {
            GET("/") {
                ok().contentType(TEXT_HTML).body(fromObject(content))
            }
        }

    }


}