import _ from 'lodash';
import $ from 'jquery'

const APPLICATION_FORM = 'application/x-www-form-urlencoded; charset=utf-8';
const DATA_TYPE = 'json';

export default class api {

    constructor() {
        this.req = req;
    }

    getReq(url, success, error) {
        this.req('GET', url, DATA_TYPE, null, null, success, error);
    }

    postReq(url, data, success, error) {
        this.req('POST', url, DATA_TYPE, data, APPLICATION_FORM, success, error);
    }

    putReq(url, data, success, error) {
        this.req('PUT', url, DATA_TYPE, data, APPLICATION_FORM, success, error);
    }

    delReq(url, data, success, error) {
        this.req('DELETE', url, DATA_TYPE, data, APPLICATION_FORM, success,
            error);
    }

}

/*
 * This method will be private method
 * If meet "CORS sync sends OPTIONS request instead of POST" issue,
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#Simple_requests
 */
function req(method, url, dataType, data, contentType, success, error) {
    let headers = {};

    return $.ajax({
        method: method,
        type: method,
        url: url,
        dataType: dataType,
        data: data,
        headers: headers,
        contentType: contentType,
        timeout: 30000, // millisecond
        success: function(response) {
            if (_.isFunction(success)) {
                success(response);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            if (_.isFunction(error)) {
                error(error);
            }
        }
    });
}
