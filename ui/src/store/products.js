import Api from '../api/api';

const state = {
    all: []
};

const actions = {
    getAllProduct({commit}) {
        let api = new Api();
        api.getReq('http://localhost:8080/api/product', (success) => {
            commit('getAllProduct', success);
        });
    },
    findById({commit}, id) {
        let api = new Api();
        api.getReq('http://localhost:8080/api/product/' + id, (success) => {
            commit('findById', success);
        });
    }
};

const mutations = {
    getAllProduct(state, products) {
        state.all = products;
    },
    findById(state, product) {
        state.all = product;
    }
};

const getters = {
    allProducts(state) {
        return state.all;
    },
    product(state) {
        return state.all;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
