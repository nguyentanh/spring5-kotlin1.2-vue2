import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/components/pages/HomePage';
import ProductDetailPage from '@/components/pages/ProductDetailPage';
import ProductNewPage from '@/components/pages/ProductNewPage';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/product/list/:id',
            name: 'product',
            component: ProductDetailPage
        },
        {
            path: '/product/new',
            name: 'productNew',
            component: ProductNewPage
        }
    ]
});
